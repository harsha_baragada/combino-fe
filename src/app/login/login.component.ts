import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  password!: string;
  email!: string;
  isEmailError!: boolean;
  isPasswordError!: boolean;
  isLoginSuccess!: boolean;
  isLoginFailure!: boolean;
  constructor() {}

  ngOnInit(): void {}

  login() {
    this.isEmailError = false;
    this.isPasswordError = false;
    this.isLoginSuccess = false;
    this.isLoginFailure = false;
    if (!this.email) {
      this.isEmailError = true;
    }
    if (!this.password) {
      this.isPasswordError = true;
    }
    if (this.email == 'john.doe@gmail.com' && this.password == '123456Asdf') {
      this.isLoginSuccess = true;
    } else {
      this.isLoginFailure = true;
    }
  }
}

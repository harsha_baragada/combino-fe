import { DataService } from './../service/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css'],
  providers: [],
})
export class DataComponent implements OnInit {
  posts: any[] = [];
  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.subscribeData();
  }

  subscribeData() {
    this.dataService.getData().subscribe((response) => {
      this.posts = response;
    });
  }
}

import { DataService } from './../service/data.service';
import { MyErrorStateMatcher } from './../service/error-matcher';
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  signupForm!: FormGroup;
  matcher = new MyErrorStateMatcher();
  constructor(private readonly service: DataService) {}

  ngOnInit(): void {
    this.signupForm = this.createSignUpForm();
  }

  createSignUpForm(): FormGroup {
    return new FormGroup({
      emailFormControl: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      passwordFormControl: new FormControl('', Validators.required),
    });
  }

  submit() {
    if (this.signupForm.valid) {
      let user = {
        email: this.signupForm.controls.emailFormControl.value,
        password: this.signupForm.controls.passwordFormControl.value,
      };
      this.service.signUp(user).subscribe(
        (response) => {
          if (response == 1) {
            alert('User signup successful.!!');
          }
        },
        (error) => {
          alert('internal server error');
        }
      );
    } else {
      alert('failure');
    }
  }
}

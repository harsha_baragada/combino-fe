import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class DataService {
  constructor(private http: HttpClient) {}

  getData(): Observable<any> {
    return this.http.get('https://jsonplaceholder.typicode.com/posts', {
      responseType: 'json',
    });
  }

  signUp(user: any): Observable<any> {
    return this.http.post('http://localhost:8080/userSignup', user);
  }
}

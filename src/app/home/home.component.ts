import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
    
  name!: string;
  fruits: string[] = ['Apple', 'Banana', 'Papaya', 'Pears', 'Strawberries'];
  showFruits!: boolean;
  buttonText: string = 'Show fruits';
  constructor() {}

  ngOnInit(): void {}

  toggleFruits() {
    this.showFruits = !this.showFruits;
    if (this.showFruits) {
      this.buttonText = 'Hide fruits';
    } else {
      this.buttonText = 'Show fruits';
    }
  }
}
